/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SecurityHuksDHBasicAbort63KBCallbackJsunit from './Agree/SecurityHuksDHBasicAbort63KBCallbackJsunit.test.js'
import SecurityHuksDHBasicAbort65KBCallbackJsunit from './Agree/SecurityHuksDHBasicAbort65KBCallbackJsunit.test.js'
import SecurityHuksDHBasicFinish63KBCallbackJsunit from './Agree/SecurityHuksDHBasicFinish63KBCallbackJsunit.test.js'
import SecurityHuksDHBasicFinish65KBCallbackJsunit from './Agree/SecurityHuksDHBasicFinish65KBCallbackJsunit.test.js'
import SecurityHuksECDHBasicAbort63KBCallbackJsunit from './Agree/SecurityHuksECDHBasicAbort63KBCallbackJsunit.test.js'
import SecurityHuksECDHBasicAbort65KBCallbackJsunit from './Agree/SecurityHuksECDHBasicAbort65KBCallbackJsunit.test.js'
import SecurityHuksECDHBasicFinish63KBCallbackJsunit from './Agree/SecurityHuksECDHBasicFinish63KBCallbackJsunit.test.js'
import SecurityHuksECDHBasicFinish65KBCallbackJsunit from './Agree/SecurityHuksECDHBasicFinish65KBCallbackJsunit.test.js'
import SecurityHuksX25519BasicAbort63KBCallbackJsunit from './Agree/SecurityHuksX25519BasicAbort63KBCallbackJsunit.test.js'
import SecurityHuksX25519BasicAbort65KBCallbackJsunit from './Agree/SecurityHuksX25519BasicAbort65KBCallbackJsunit.test.js'
import SecurityHuksX25519BasicFinish63KBCallbackJsunit from './Agree/SecurityHuksX25519BasicFinish63KBCallbackJsunit.test.js'
import SecurityHuksX25519BasicFinish65KBCallbackJsunit from './Agree/SecurityHuksX25519BasicFinish65KBCallbackJsunit.test.js'
export default function testsuite() {
SecurityHuksDHBasicAbort63KBCallbackJsunit()
SecurityHuksDHBasicAbort65KBCallbackJsunit()
SecurityHuksDHBasicFinish63KBCallbackJsunit()
SecurityHuksDHBasicFinish65KBCallbackJsunit()
SecurityHuksECDHBasicAbort63KBCallbackJsunit()
SecurityHuksECDHBasicAbort65KBCallbackJsunit()
SecurityHuksECDHBasicFinish63KBCallbackJsunit()
SecurityHuksECDHBasicFinish65KBCallbackJsunit()
SecurityHuksX25519BasicAbort63KBCallbackJsunit()
SecurityHuksX25519BasicAbort65KBCallbackJsunit()
SecurityHuksX25519BasicFinish63KBCallbackJsunit()
SecurityHuksX25519BasicFinish65KBCallbackJsunit()
}
