/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import AbilityStage from "@ohos.application.AbilityStage"

var onAcceptWantCalledSeq;

export default class MyAbilityStage extends AbilityStage {
    onCreate() {
        console.log("AbilityMultiInstanceAppD MyAbilityStage onCreate")
        globalThis.stageOnCreateRun = 1;
        globalThis.stageContext = this.context;
    }

    onAcceptWant(want) {
        console.log("AbilityMultiInstanceAppD onAcceptWant called want:" + JSON.stringify(want));
        var abilityId = want.parameters.startId.toString();
        onAcceptWantCalledSeq = "onAcceptWantCalled";
        onAcceptWantCalledSeq += "Id";
        onAcceptWantCalledSeq += abilityId;
        globalThis.onAcceptWantCalledSeq = onAcceptWantCalledSeq;
        globalThis.abilityWant = want;
        console.log("AbilityMultiInstanceAppD set globalThis result :" + globalThis.onAcceptWantCalledSeq);
        return abilityId;
    }
}